sudo apt-get update

#install git
sudo apt-get install git

#install Lightweight X? Desktop Environment
sudo apt-get install lubuntu

#install java 8
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update --fix-missing
sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

#install Android SDK and Android Studio
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
sudo apt-get update
sudo apt-get install ubuntu-make
umake android

#install Chrome
#1st, install the Google Linux Repo key
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
#2nd, add key to the repository
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

sudo apt-get update
sudo apt-get install google-chrome-stable