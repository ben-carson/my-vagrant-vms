sudo apt-get update

mkdir ~/dev
cd dev
mkdir code
mkdir assets
mkdir tools

#install vim
sudo apt-get --yes install vim

#install git
sudo apt-get --force-yes --yes install git

#install svn client
sudo apt-get --yes install svn

#install Lightweight X? Desktop Environment
#sudo apt-get --yes install lxde

#install java 8
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update --fix-missing
sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get --yes install oracle-java8-installer

#install Chrome
#1st, install the Google Linux Repo key
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
#2nd, add key to the repository
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

sudo apt-get update
sudo apt-get --yes install google-chrome-stable

#install Thunderbird
sudo apt-get --yes install thunderbird

#install Inkscape
sudo apt-get --yes install inkscape

#install GIMP
sudo add-apt-repository ppa:otto-kesselgulasch/gimp
sudo apt-get update
sudo apt-get --yes install gimp

#————Install Blender (the pre-compiled version)————
wget http://download.blender.org/release/Blender2.73/blender-2.73-linux-glibc211-x86_64.tar.bz2 -O ~/Downloads/blender-2.73.tar.z2

tar -xjvf ~/Downloads/blender-2.73.tar.bz2 -C ~/Downloads/

sudo mv ~/Downloads/blender-2.73-linux-glibc211-x86_64/ /usr/bin/blender-2.73/

printf "[Desktop Entry]\nEncoding=UTF-8\nName=Blender 2.73\nComment=Blender 3D\nExec=/usr/bin/blender-2.73/blender\nIcon=/usr/bin/blender-2.73/icons/48x48/apps/blender.png\nCategories=Application\nVersion=1.0\nType=Application\nTerminal=0" > ~/Desktop/"Blender.desktop"


#—————Install Blender (the latest version by building)—————
#http://wiki.blender.org/index.php/Dev:Doc/Building_Blender/Linux/Ubuntu/CMake
#sudo apt-get build-dep blender
#mkdir /home/vagrant/blender-git
#cd /home/vagrant/blender-git
#git clone http://git.blender.org/blender.git
#cd blender
#git submodule update --init --recursive
#git submodule foreach git checkout master
#git submodule foreach git pull --rebase origin master
#to update to the LATEST source:
#git pull --rebase
#git submodule foreach git pull --rebase origin master

#git build-essential libxi-dev libsndfile1-dev libopenexr-dev libopenjpeg-dev libpng12-dev libjpeg-dev libopenal-dev libalut-dev python3.4-dev libglu1-mesa-dev libsdl-dev libfreetype6-dev libtiff4-dev libavdevice-dev -libavformat-dev -libavutil-dev -libavcodec-dev libswscale-dev libx264-dev -libxvidcore4-dev(unable to locate) libxvidcore4-dev libmp3lame-dev libspnav-dev -python3.4

#cd ~/blender-git/blender
#python scons/scons.py -j 6

#cd ~/blender-git/blender
#ln -s ../install/linux2/blender ./blender

#————-end failed Blender experiment———-


#Install Eclipse
wget http://carroll.aset.psu.edu/pub/eclipse/technology/epp/downloads/release/luna/SR1a/eclipse-jee-luna-SR1a-linux-gtk-x86_64.tar.gz -O ~/Downloads/eclipse-luna.tar.gz
tar -xzvf ~/Downloads/eclipse-luna.tar.gz —-directory ~/Downloads/
sudo mv ~/Downloads/eclipse /usr/bin/
printf “[Desktop Entry]\nEncoding=UTF-8\nName=Eclipse Luna\nComment=RunEclipseIDE\nIcon=/usr/bin/eclipse/icon.xpm\nExec=/usr/bin/eclipse/eclipse\nTerminal=false\nType=Application” > ~/Desktop/“Eclipse Luna.desktop”

#Install libGDX
mkdir ~/dev/tools/LibGDX
wget http://bitly.com/1i3C7i3 -O ~/dev/tools/LibGDX/libgdx-setup.jar
#wget https://github.com/libgdx/libgdx/blob/master/libgdx_logo.svg -O ~/dev/tools/LibGDX/libgdx-logo.svg
printf "[Desktop Entry]\nEncoding=UTF-8\nName=Create LibGDX Game\nComment=Create a new game project\nIcon=gnome-terminal\nExec=java -jar ~/dev/tools/LibGDX/libgdx-setup.jar --dir ~/dev/projects --package com.bencarson.game\nTerminal=false\nType=Application\nPath=/home/vagrant/dev/projects" > ~/Desktop/"Create New LibGDX Game.desktop"


#install jMonkeyEngine
wget http://jmonkeyengine.org/?ddownload=301324 -O ~/Downloads/jMonkeyPlatform-x64.sh
#Not sure how to install this. It has a wizard


#install MakeHuman
sudo apt-get -f install —-yes
wget http://download.tuxfamily.org/makehuman/releases/1.0.2/makehuman-1.0.2_all.deb -O ~/Downloads/makehuman-1.0.2.deb
sudo dpkg -i ~/Downloads/makehuman-1.0.2.deb