sudo apt-get update

#install curl
wget http://curl.haxx.se/download/curl-7.39.0.tar.gz -P /vagrant_data
tar -xzvf /vagrant_data/curl-7.37.1.tar.gz
cd /vagrant_data/curl-7.37.1/
./configure
make
sudo make install

#install the NodeJS PPA, in order to get more recent versions of Node than
#what is housed in the Ubuntu repositories
#curl -sL https://deb.nodesource.com/setup | sudo bash -
#install node
#sudo apt-get install nodejs
#install npm
#sudo apt-get install npm

#install grunt
#install ruby
#install SASS
#install Compass
#install git
#install Brackets
#install Thunderbird
#install Chrome
#install Safari
#install Opera

#install docker
#activate lxd container manager
sudo lxd init
