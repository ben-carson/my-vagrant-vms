### Collection of Virtual Machines
---
This project is merely a collection of Vagrant files that will build the various VMs that use. I have seperated the VMs out by use case, because that makes sense to me. Game-dev, android-dev, web-dev, etc.

I should add more details about each type of vm here at some point.

_Game-dev_:
* Thunderbird
* svn
* git
* Java
* ...

_Android-dev_:
* Thunderbird
* git
* Java
* ...

-web-dev_:
* Brackets
* git
* chrome
* ...
